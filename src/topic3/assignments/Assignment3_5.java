package topic3.assignments;

import java.util.Scanner;

public class Assignment3_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the string: ");
		String str = sc.nextLine();
		System.out.println("Enter the character ");
		char c = sc.nextLine().charAt(0);
		int charCount = str.length() - str.replaceAll(Character.toString(c), "").length();
		System.out.println("The number of occurences of "+ c + " is "+charCount);
	}

}
