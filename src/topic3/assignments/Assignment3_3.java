package topic3.assignments;

public class Assignment3_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = new int[5];
		try {
		for(int i = 0; i < arr.length; i ++) {
			if(!args[i].isEmpty())
				arr[i] = Integer.parseInt(args[i]);
			else
				throw new ArrayIndexOutOfBoundsException();
		}
		int sum = 0;
		for(int k:arr) {
			sum += k;
		}
		System.out.println("The average is "+ sum/5);
		}
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("Kindly enter 5 numbers");
		}

	}

}
