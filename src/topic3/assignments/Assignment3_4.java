package topic3.assignments;

import java.util.Scanner;

public class Assignment3_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String str = sc.next();
		StringBuffer s = new StringBuffer(str);
		if(s.reverse().toString().equalsIgnoreCase(str)) {
			System.out.println("The string is a palindrome");
		}
		else {
			System.out.println("The string is not a palindrome");
		}

	}

}
