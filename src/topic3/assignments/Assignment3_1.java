package topic3.assignments;

public class Assignment3_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String name = args[0];
		int age = Integer.parseInt(args[1]);
		try {
			if(age < 18 || age > 60)throw new AgeException();
			else
				System.out.println("Name: "+ name + " Age:"+ age);
		}
		catch(AgeException e) {
			System.out.println("The age should be between 18 and 60");
		}

	}

}

class AgeException extends Exception{
	
}
