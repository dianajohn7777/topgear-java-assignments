package topic3.assignments;

public class Assignment3_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int k = 2;
		String name1 = args[0];
		String name2 = args[1];
		int[] arr1 = new int[6];
		try {
		for(int i = 0; i < arr1.length; i++) {
			arr1[i] = Integer.parseInt(args[k]);
			k++;
		}
		int sum1=0,sum2=0;
		for(int i = 0; i < arr1.length; i++) {
			if(i<3) {
				sum1+=arr1[i];
			}
			else {
				sum2+=arr1[i];
			}
		}
		System.out.println("The average mark of "+ name1+" is "+ sum1/3);
		System.out.println("The average mark of "+ name2+" is "+ sum2/3);
		}
		catch(NumberFormatException e) {
			System.out.println("Enter a valid number");
		}

	}

}
