package topic5.assignments.automobile.twowheeler;

import topic5.assignments.automobile.Vehicle;

class Hero extends Vehicle{
	public String modelName() {
		return "Splendor Plus";
	}
	public String registrationNumber() {
		return "KA 03 JK 6785";
	}
	public String ownerName() {
		return "lkjh";
	}
	public int speed() {
		return 80;
	}
	public void radio() {
		System.out.println("Radio can be controlled");
	}

}
class Honda extends Vehicle{
	public String modelName() {
		return "Activa-5G";
	}
	public String registrationNumber() {
		return "TN 23 AD 7897";
	}
	public String ownerName() {
		return "djap";
	}
	public int speed() {
		return 70;
	}
	public int cdplayer() {
		return 100;
	}

}

public class Assignment5_3{
	public static void main(String[] args) {
		
		Hero hero = new Hero();
		System.out.println("The details of Hero are as below:");
		System.out.println(hero.modelName()+"\t"+hero.registrationNumber()+"\t"+hero.ownerName()+"\t"+hero.speed());
        hero.radio();
        Honda honda = new Honda();
        System.out.println("The details of Honda are as below: ");
        System.out.println(honda.modelName()+"\t"+honda.registrationNumber()+"\t"+honda.ownerName()+"\t"+honda.speed()+"\t"+honda.cdplayer());
	}
}
