package topic1.assignments;

import java.util.Scanner;

public class Assignment5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		int num = sc.nextInt();
		int sum = 0;
		while(num!=0) {
			sum += num % 10;
			num /= 10; 
		}
		System.out.println("The sum of the digits is: " + sum);

	}

}
