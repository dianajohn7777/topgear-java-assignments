package topic1.assignments;

import java.util.Scanner;

public class Assignment7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter a number: ");
		Scanner sc  = new Scanner(System.in);
		int num = sc.nextInt();
		System.out.println("The factorial of the number: "+ factorial(num));
		

	}
	static int factorial(int number) {
		if( number == 0 || number == 1) return 1;
		else
			return number * factorial(number - 1);
	}

}
