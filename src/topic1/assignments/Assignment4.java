package topic1.assignments;

import java.util.Scanner;

public class Assignment4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the month in numbers");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		String inWords = "";
		switch(n) {
		case 1: inWords = "January"; break;
		case 2: inWords = "February"; break;
		case 3: inWords = "March"; break;
		case 4: inWords = "April"; break;
		case 5: inWords = "May"; break;
		case 6: inWords = "June"; break;
		case 7: inWords = "July"; break;
		case 8: inWords = "August"; break;
		case 9: inWords = "September"; break;
		case 10: inWords = "October"; break;
		case 11: inWords = "November"; break;
		case 12: inWords = "December"; break;
		default: inWords = "Invalid month";
		}
		System.out.println(inWords);

	}

}
