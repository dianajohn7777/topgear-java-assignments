package topic2.live;

import topic2.music.Playable;
import topic2.music.string.Veena;
import topic2.music.wind.Saxaphone;

public class Test {

	public static void main(String[] args) {
		Veena obj1 = new Veena();
		obj1.play();
		Saxaphone obj2 = new Saxaphone();
		obj2.play();
		
		Playable var = obj1;
		var.play();
		
		Playable var1 = obj2;
		var1.play();
		


	}

}
