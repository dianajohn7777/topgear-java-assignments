package topic2.assignments;

// Assignment 3

class Book1 {
	private String isbn;
	private String title;
	private double price;
	public Book1(String isbn, String title, double price) {
		this.isbn = isbn;
		this.title = title;
		this.price = price;
	}
	void displayDetails() {
		System.out.println("The isbn number : "+ isbn);
		System.out.println("The title of the book : "+ title);
		System.out.println("The price of the book : "+ price);
	}
	
}

class Magazine extends Book1{
	private String type;
	
	Magazine(String isbn, String title, double price,String type){
		super(isbn,title,price);
		this.type = type;
		
	}
	void displayDetails(){
		super.displayDetails();
		System.out.println("The type of the magazine : "+ type);
	}
}

class Novel extends Book1{
	private String author;
	
	Novel(String isbn, String title, double price,String author){
		super(isbn,title,price);
		this.author = author;
		
	}
	void displayDetails(){
		super.displayDetails();
		System.out.println("The author of the novel : "+ author);
	}
}

public class Result{
	public static void main(String[] args) {
		Magazine obj = new Magazine("345-KL-90","Open Source",150,"Tech");
		Novel obj1 = new Novel("44-LKJ-908","The Lost Symbol",300,"Dan Brown");
	    obj.displayDetails();
	    System.out.println();
	    obj1.displayDetails();
	}
}



