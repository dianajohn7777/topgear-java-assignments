package topic2.assignments;

// Assignment 1
	public class Book{
		public String isbn;
		public String title;
		public String author;
		public double price;
		
		Book(String isbn, String title, String author, double price){
			this.isbn = isbn;
			this.title = title;
			this.author = author;
			this.price = price;
		}
		
		void displaydetails() {
			System.out.println("The isbn number : "+ isbn);
			System.out.println("The title of the book : "+ title);
			System.out.println("The author of the book : "+ author);
			System.out.println("The price of the book : "+ price);
		}
		void discountedprice(int discount) {
			double amt = price - price * (discount / 100);
		    System.out.println("The discounted amount is:" + amt);
		}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Book obj = new Book("123-JIT-4567","Java Programming","O'Brien",1200);
        obj.displaydetails();
        obj.discountedprice(20);
	}

}
