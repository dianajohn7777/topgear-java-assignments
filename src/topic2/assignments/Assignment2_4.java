package topic2.assignments;

import java.util.Date;

// Assignment 4
 class Payment{
	
	private double amt;
	
	Payment(double amt){
		this.amt = amt;
	}
	
	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}
	
	public void paymentDetails() {
		System.out.println("The total amount of payment done: "+ amt);
	}

}

class CashPayment extends Payment{
	
	CashPayment(double amt){
		super(amt);
	}
	
	public void paymentDetails() {
		System.out.println("The payment is in cash: "+ getAmt());
	}
}

class CreditCardPayment extends Payment{
	
	private String cardName;
	private String expirationDate;
	private long cardNum;
	
	public CreditCardPayment(double amt,String cardName, String expirationDate, long cardNum) {
		super(amt);
		this.cardName = cardName;
		this.expirationDate = expirationDate;
		this.cardNum = cardNum;
	}
	
	public void paymentDetails() {
		System.out.println("The name on the card is: "+ cardName);
		System.out.println("The expiration date is: "+ expirationDate);
		System.out.println("The card number is: "+ cardNum);
	}
	
}

public class Assignment2_4{
	public static void main(String[] args) {
		Payment obj1 = new CashPayment(1500);
		obj1.paymentDetails();
		CashPayment obj2 = new CashPayment(50000);
		obj2.paymentDetails();
		Payment obj3 = new CreditCardPayment(10000,"XYZ","01/01/2020",98756456);
		obj3.paymentDetails();
		CreditCardPayment obj4 = new CreditCardPayment(70000, "PQR", "08/09/2021", 8373632);
		obj4.paymentDetails();
		
	}
}

