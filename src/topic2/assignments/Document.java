package topic2.assignments;
// Assignment 2
public class Document {
	
	public String text;


	public String toString() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}

}

class Email extends Document{
	private String sender;
	private String recipient;
	private String title;
	private String body;
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setBody(String body) {
		super.text = body;
	}
	
	@Override
	public String toString() {
		return sender + recipient + title + body;
	}
}
