package topic2.assignments;

abstract class Instrument{
	abstract void play();
}

class Piano extends Instrument{

	@Override
	void play() {
		System.out.println("Piano is playing tan tan tan ");
		
	}
	
}
class Flute extends Instrument{

	@Override
	void play() {
		System.out.println("Flute is playing toot toot toot toot");
	}
	
}
class Guitar extends Instrument{

	@Override
	void play() {
		System.out.println("Guitar is playing tin tin tin");
	}
	
}

public class Assignment2_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Instrument[] arr = new Instrument[10];
		arr[0] = new Piano();
		arr[1] = new Guitar();
		arr[2] = new Flute();
		arr[3] = new Guitar();
		arr[4] = new Piano();
		arr[5] = new Piano();
		arr[6] = new Flute();
		arr[7] = new Guitar();
		arr[8] = new Flute();
		arr[9] = new Guitar();
		
		arr[0].play();
		arr[3].play();
		arr[8].play();
		
		System.out.println();
		int index = 1;
		
		for(Instrument obj: arr) {
			if(obj instanceof Piano) {
				System.out.println("Piano is instrument number: "+ index);
			}
			if(obj instanceof Guitar) {
				System.out.println("Guitar is instrument number: "+ index);
			}
			if(obj instanceof Flute) {
				System.out.println("Flute is instrument number: "+ index);
			}
			index ++;
		}

	}

}
