package topic4.assignments;

class Test1 implements Runnable{
	Thread t;
	int num;
	Test1(int num){
		t = new Thread();
		this.num = num;
	}
	public void run() {
		System.out.println("Number :"+num);
	}
}

class Test2 implements Runnable{
	Thread t1;
	int num;
	Test2(int num){
		t1 = new Thread();
		this.num = num;
	}
	public void run() {
		System.out.println("Factorial of "+num+" : "+factorial(num));
	}
	public int factorial(int num) {
		if(num==0 || num==1)return 1;
		else
			return num * factorial(num-1);
	}
}

public class Assignment4_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for(int i=0;i<5;i++) {
			int num =(int) (Math.random()*10);
			Thread T1 = new Thread(new Test1(num));
			Thread T2 = new Thread(new Test2(num));
			T1.start();
			T2.start();
		}

	}

}
