package topic4.assignments;

import java.util.*;
import java.util.Map.Entry;

public class Assignment4_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<String,String> map = new HashMap<String,String>();
		map.put("Abi","91-9386372345");
		map.put("Sakthi", "91-7849367849");
		map.put("Shiva", "91-9475823741");
		map.put("Guru", "91-8925301118");
		
		Set set = map.entrySet();
		System.out.println("Enter a name: ");
		Scanner sc = new Scanner(System.in);
		String s = sc.next();
		Iterator it = set.iterator();
		while(it.hasNext()) {
			Map.Entry obj = (Entry) it.next();
			if(obj.getKey().equals(s)) {
				System.out.println("The phone number of "+obj.getKey()+" is "+obj.getValue());
			}
		}
	}

}
