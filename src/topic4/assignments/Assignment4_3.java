package topic4.assignments;

import java.util.*;

class Employee{
	private String name;
	private int empId;
	private double salary;
	Employee(){
		
	}
	Employee(String name,int empId, double salary){
		this.name = name;
		this.empId = empId;
		this.salary = salary;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}	
	public String getEmpSalary(int empId) {
		return "The salary of "+ getName() +" is "+ getSalary();
	}
	
	public void getDetails() {
		System.out.println("Name: "+name+" Employee ID: "+empId+" Salary: "+salary);
	}
}
class EmployeeDB{
	ArrayList<Employee> result;
	EmployeeDB(ArrayList<Employee> emp){
		result = emp;
	}
	boolean addEmployee(Employee e) {
		result.add(e);
		return true;
	}
	boolean deleteEmployee(int eCode) {	
		Iterator<Employee> itr = result.iterator();
		while(itr.hasNext()) {
			Employee e = itr.next();
			if(e.getEmpId()== eCode) {
				result.remove(e);
				return true;
			}
		}
		return false;
	}
	String showPaySlip(int eCode) {
		Iterator<Employee> itr = result.iterator();
		while(itr.hasNext()) {
			Employee e = itr.next();
			if(e.getEmpId()== eCode) {
				return e.getEmpSalary(eCode);
			}
		}
		return "";
	}
	Employee[] listAll() {
		Employee[] a = new Employee[result.size()];
		int i = 0;
		Iterator<Employee> itr = result.iterator();
		while(itr.hasNext()) {
			a[i] = itr.next();
			i++;
		}
		return a;
	}
}
public class Assignment4_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Employee> arr = new ArrayList<Employee>();
        EmployeeDB obj = new EmployeeDB(arr);
        obj.addEmployee(new Employee("Karthik",789635,35786));
        obj.addEmployee(new Employee("Shivani",678932,42000));
        obj.addEmployee(new Employee("Gautam",234532,60000));
        obj.addEmployee(new Employee("Keerthi",389323,29763));
        Employee[] empl = obj.listAll();
        for(Employee e: empl) {
        	e.getDetails();
        }
        System.out.println();
        obj.deleteEmployee(678932);
        
        System.out.println(obj.showPaySlip(789635));
        System.out.println();
        
        Employee[] empl1 = obj.listAll();
        for(Employee e: empl1) {
        	e.getDetails();
        }
        
	}

}
